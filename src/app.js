const request = fetch('http://dev.drupal-graphql/graphql',{
    method: 'POST',
    headers: { "Content-Type": "application/json" },
    body: JSON.stringify({ query: `{
      nodeQuery(limit: 3) {
        entities {
          ... on Node{
            title
            created
          }
        }
      }
    }` })
  })
  .then(response => response.json())
  .then(result => result);

const renderItem = (item) => {
  const resultsDiv = document.querySelector('#results');
  const row = document.createElement('div');
  const dateNumber = Number(item.created)*1000;
  const dateFormated = new Date(dateNumber);
  row.innerHTML = `<div><h4>${item.title}</h4><p>${dateFormated}</p></div>`;
  resultsDiv.append(row);
}

request.then(res => res.data.nodeQuery.entities.forEach(item => renderItem(item)));