const path = require('path');

module.exports = {
    entry: './src/app.js',
    output: {
        filename: 'app.js',
        path: path.resolve(__dirname, 'build')
    },
    module: {
        rules: [
              { test: /\.js$/, exclude: /node_modules/, loader: "babel-loader" }
        ]
    }
}